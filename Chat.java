import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.lang.*;
import javax.swing.JOptionPane;

//ditentukan dari port 23450

public class Chat
{
	private static Algorithm prev;
	private static Algorithm next;
	private static String nama;
	private static final int baseport = 23450;
	private static Integer jmlOrang;

	public static void peerServer() throws Exception
	{
		nama = JOptionPane.showInputDialog("Masukkan Nama anda");
		boolean isDone = false;		
		
		while (!isDone)
		{
			String input = JOptionPane.showInputDialog("Masukkan Jumlah orang");
			jmlOrang = Integer.parseInt(input);
			if (jmlOrang > 1) isDone = true;
		}
	
		//Listen untuk next peer
		ServerSocket serverSocket = new ServerSocket(baseport);
		System.out.println("Listening Next Peer @"+baseport+". . .");
		Socket server = serverSocket.accept();
		next = new Algorithm(server);
		next.sendInt(baseport+jmlOrang-1); //kirim port destination

		//listen untuk prev peer, jika sudah masuk. Maka chat bisa berlangsung
		ServerSocket serverSocket2 = new ServerSocket(baseport+jmlOrang-1);
		System.out.println("Next Peer telah terhubung");
		System.out.println("Listening Last Peer @"+(baseport+jmlOrang-1)+". . .");
		Socket server2 = serverSocket2.accept();
		prev = new Algorithm(server2);		
		
		//penanda chat bisa dimulai
		next.send("MULAI"); //data dummy penanda
		String data = prev.recv(); //menerima data dummy

		System.out.println("Chat bisa dimulai. . .");
		//program chat
		Scanner sc = new Scanner(System.in);
		prev.setNext(next);
		prev.setPort(baseport);
		prev.start();	
		while (true)
		{
			next.send(nama+" says : "+sc.nextLine());
			next.sendInt(baseport);
		}		
	}

	public static void peerClient() throws Exception
	{
		nama = JOptionPane.showInputDialog("Masukkan Nama anda");
		String IP = JOptionPane.showInputDialog("Masukkan IP Address");
		Integer Port = Integer.parseInt(JOptionPane.showInputDialog("Masukkan Port peer yang terbuka"));

		Socket client = new Socket(IP,Port);
		prev = new Algorithm(client); //terkoneksi dengan peer tujuan
		Integer PortDest = prev.recvInt(); //mengambil base port

		System.out.println("Port : "+(Port+1)+". PortDest = "+PortDest);
		if (PortDest == Port+1)
		{
			Socket client2 = new Socket(IP,PortDest);
			next = new Algorithm(client2);
		}
		else
		{
			ServerSocket serverSocket2 = new ServerSocket(Port+1);
			System.out.println("Listening Peer @"+(Port+1)+". . .");
			Socket server2 = serverSocket2.accept();
			next = new Algorithm(server2);		
			next.sendInt(PortDest); //kirim port destination
		}	

		String data = prev.recv(); //penerima mulai
		next.send(data); //mengirimkan juga

		System.out.println("Chat bisa dimulai. . .");

		//program chat
		Scanner sc = new Scanner(System.in);
		prev.setNext(next);
		prev.setPort(Port+1);	
		prev.start(); //dari prev seluruh data	
		while (true)
		{
			next.send(nama+" says : "+sc.nextLine());
			next.sendInt(Port+1);
		}
	}

	public static void main(String args[])
	{
		System.out.println("==========================================");
		System.out.println("Pilihan anda ");
		System.out.println("==========================================");
		System.out.println("1. peerServer");
		System.out.println("2. peerClient");
		System.out.println("==========================================");
		
		try
		{
			Scanner sc = new Scanner(System.in);
			System.out.print("Masukkan pilihan anda : "); int pil = sc.nextInt();
			if (pil == 1) peerServer();
			else if (pil == 2) peerClient();
			else System.out.println("Pilihan Salah");
		}catch (Exception e){System.out.println("Error : "+e.getMessage());}
	}
}
