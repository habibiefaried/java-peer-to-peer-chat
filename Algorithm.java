/*****
Created By : Habibie Faried
Date : 18 Juli 2013
Deskripsi : File ini merupakan algoritma socket dasar yang digunakan untuk
membuat program Chat Non-Blocking lebih dari 2 User
******/

import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.lang.*;

class Algorithm extends Thread
{
	private Socket socket;
	private Algorithm next; //buat kirim ke next socket
	private int Port;

	public void setPort(int Port){this.Port = Port;}
	public void setNext(Algorithm A){next = A;}

	public Algorithm(Socket socket)
	{
		this.socket = socket;
	}

	public void send(String S)
	{
		//Prosedur ini untuk mengirimkan data berupa string
		try
		{
			DataOutputStream out =  new DataOutputStream(socket.getOutputStream());
		   out.writeUTF(S);		
		}	
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public void sendInt(Integer I)
	{
		//Prosedur ini untuk mengirimkan data berupa string dengan parameter integer
		try
		{
			DataOutputStream out =  new DataOutputStream(socket.getOutputStream());
		   out.writeUTF(I.toString());		
		}	
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public String recv()
	{
		//Prosedur ini untuk menerima data berupa string, mengembalikan string
		try
		{
			DataInputStream in = new DataInputStream(socket.getInputStream());
			return in.readUTF();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		return "Error saat menerima data";
	}

	public Integer recvInt()
	{
		//Prosedur ini untuk menerima data berupa string, mengembalikan Integer
		try
		{
			DataInputStream in = new DataInputStream(socket.getInputStream());
			return Integer.parseInt(in.readUTF());
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		return -9999;
	}

	public void run()
	{
		//Untuk menerima data non-blocking digunakan thread
		//Prosedur ini untuk menerima teks chat
		String data;
	 	while (true)
		{
			try
			{
				DataInputStream in = new DataInputStream(socket.getInputStream());
				data = in.readUTF();
				Integer I = recvInt(); //recv int
				
				if (Port != I) //jika port tidak sama
				{
					System.out.println(data); //recv data
					next.send(data);
					next.sendInt(I);
				}

			}
			catch (Exception e)
			{
				System.out.println("Lawan bicara offline, keluar dari program. . .");
				System.exit(0);
			}
		}
	}
}
